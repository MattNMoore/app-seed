<html>
    <head>
        <title>Moorebread</title>

        <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
    </head>

    <body>
        @yield('content')

        <script type="text/javascript" href="assets/vendor/"></script>
    </body>
</html>
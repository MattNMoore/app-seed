## App Seed

Starter app with Laravel 5, Sass Bootstrap, AngularJS, Gulp, and Bower

``` gulp:watch ```
Build and watch for changes in a dev environment. Transfers files without concatenation or uglify to /public

``` gulp:build ```
Build for production. Concatenates and uglifies Sass and JS.

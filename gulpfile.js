var gulp   = require('gulp')
    sass   = require('gulp-sass')
    concat = require('gulp-concat')
    uglify = require('gulp-uglify')
    watch  = require('gulp-watch')

gulp.task('build:sass', function()
{
    return gulp.src('resources/sass/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('public/assets/css/'));
})

gulp.task('build:dev', function()
{
    return gulp.src(['resources/vendor/jquery/dist/jquery.js'])
        .pipe(gulp.dest('public/assets/js/'));
});